//
//  HomeVC.swift
//  SiteSense AI
//
//  Created by Nick Joliya on 30/09/23.
//

import UIKit

class HomeVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    @IBAction func btnAIForCoder(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AIForCoder") as! AIForCoder
        vc.data = AIDataManager.shared.getCoderAIModels()
        vc.titleString = "AI For Coder"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnContentCreator(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AIForCoder") as! AIForCoder
        vc.data = AIDataManager.shared.getContentCreatorAIModels()
        vc.titleString = "AI for Content Creator"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func imageGenerator(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AIForCoder") as! AIForCoder
        vc.data = AIDataManager.shared.getImageAIModels()
        vc.titleString = "AI For Image generate"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnVoice(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AIForCoder") as! AIForCoder
        vc.data = AIDataManager.shared.getVoiceAIModels()
        vc.titleString = "AI For Voice"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnLogo(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AIForCoder") as! AIForCoder
        vc.data = AIDataManager.shared.getLogoAIModels()
        vc.titleString = "Create Logo"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnCreateAdds(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AIForCoder") as! AIForCoder
        vc.data = AIDataManager.shared.getAdAIModels()
        vc.titleString = "Create Ads"
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    @IBAction func btnAnimation(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AIForCoder") as! AIForCoder
        vc.data = AIDataManager.shared.getAnimationAIModels()
        vc.titleString = "Create Animations"
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    @IBAction func btnSubtitle(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AIForCoder") as! AIForCoder
        vc.data = AIDataManager.shared.getSubtitleAIModels()
        vc.titleString = "Create Reel Subtitle"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnMemes(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AIForCoder") as! AIForCoder
        vc.data = AIDataManager.shared.getMemesAIModels()
        vc.titleString = "Create Memes"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnVideoEditing(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AIForCoder") as! AIForCoder
        vc.data = AIDataManager.shared.getVideoEditingAIModels()
        vc.titleString = "Video Editing"
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func btnScriptToVideo(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AIForCoder") as! AIForCoder
        vc.data = AIDataManager.shared.getscriptToVideoAIModels()
        vc.titleString = "Script To Video"
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func btnAboutApp(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AboutVC") as! AboutVC
       
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnContact(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
       
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
