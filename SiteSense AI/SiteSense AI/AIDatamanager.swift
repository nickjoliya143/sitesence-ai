//
//  AIDatamanager.swift
//  TeeFusion Gallery
//
//  Created by Nick Joliya on 30/09/23.
//

struct AIModel {
    let name: String
    let image: String
    let link: String
}

class AIDataManager {
    static let shared = AIDataManager()
    
    private init() {
        // Private initializer to enforce singleton pattern
    }
    
    func getCoderAIModels() -> [AIModel] {
        var models = [AIModel]()
        
        let model1 = AIModel(name: "ChatGPT", image: "p1", link: "https://chat.openai.com/")
        let model2 = AIModel(name: "OpenAI Codex", image: "p1", link: "https://openai.com/blog/openai-codex")
        let model3 = AIModel(name: "Tabnine", image: "p1", link: "https://www.tabnine.com/")
        let model4 = AIModel(name: "WPCode", image: "p1", link: "https://wpcode.com/")
        let model5 = AIModel(name: "Polycoder", image: "p1", link: "https://openai.com/blog/openai-codex")
        let model6 = AIModel(name: "Deepcode", image: "p1", link: "https://www.tabnine.com/")
    
        // Add more AI models as needed
        
        models.append(contentsOf: [model1, model2, model3,model4, model5, model6])
        return models
    }
    func getContentCreatorAIModels() -> [AIModel] {
        var models = [AIModel]()
        
        let model1 = AIModel(name: "ClickUp", image: "contentc", link: "https://chat.openai.com/")
        let model2 = AIModel(name: "Narrato", image: "contentc", link: "https://openai.com/blog/openai-codex")
        let model3 = AIModel(name: "Lately", image: "contentc", link: "https://www.tabnine.com/")
        let model4 = AIModel(name: "Jasper", image: "contentc", link: "https://wpcode.com/")
        let model5 = AIModel(name: "Copy.ai", image: "contentc", link: "https://openai.com/blog/openai-codex")
        let model6 = AIModel(name: "Synthesia", image: "contentc", link: "https://www.tabnine.com/")
        let model7 = AIModel(name: "Murf", image: "contentc", link: "https://wpcode.com/")
        let model8 = AIModel(name: "Canva", image: "contentc", link: "https://openai.com/blog/openai-codex")
        let model9 = AIModel(name: "Podcastle", image: "contentc", link: "https://www.tabnine.com/")
        let model10 = AIModel(name: "Beautiful.ai", image: "contentc", link: "https://www.tabnine.com/")
    
        // Add more AI models as needed
        
        models.append(contentsOf: [model1, model2, model3,model4, model5, model6, model7,model8, model9, model10])
        return models
    }
    func getImageAIModels() -> [AIModel] {
        var models = [AIModel]()
        
        let model1 = AIModel(name: "Freepik", image: "ai-application", link: "https://chat.openai.com/")
        let model2 = AIModel(name: "Fotor", image: "ai-application", link: "https://openai.com/blog/openai-codex")
        let model3 = AIModel(name: "DALL·E 2", image: "ai-application", link: "https://www.tabnine.com/")
        let model4 = AIModel(name: "Canva", image: "ai-application", link: "https://wpcode.com/")
        let model5 = AIModel(name: "Laxica", image: "ai-application", link: "https://openai.com/blog/openai-codex")
        // Add more AI models as needed
        
        models.append(contentsOf: [model1, model2, model3,model4, model5])
        return models
    }
    func getVoiceAIModels() -> [AIModel] {
        var models = [AIModel]()
        
        let model1 = AIModel(name: "Play.ht", image: "ai-voice-generator", link: "https://chat.openai.com/")
        let model2 = AIModel(name: "Murf.AI", image: "ai-voice-generator", link: "https://openai.com/blog/openai-codex")
        let model3 = AIModel(name: "Listnr", image: "ai-voice-generator", link: "https://www.tabnine.com/")
        let model4 = AIModel(name: "Speechify", image: "ai-voice-generator", link: "https://wpcode.com/")
        let model5 = AIModel(name: "LOVO", image: "ai-voice-generator", link: "https://openai.com/blog/openai-codex")
        let model6 = AIModel(name: "Synthesys", image: "ai-voice-generator", link: "https://www.tabnine.com/")
        let model7 = AIModel(name: "Resemble.AI", image: "ai-voice-generator", link: "https://wpcode.com/")
        let model8 = AIModel(name: "Clipchamp", image: "ai-voice-generator", link: "https://openai.com/blog/openai-codex")
       
    
        // Add more AI models as needed
        
        models.append(contentsOf: [model1, model2, model3,model4, model5, model6, model7,model8])
        return models
    }
    
    func getLogoAIModels() -> [AIModel] {
        var models = [AIModel]()
        
        let model1 = AIModel(name: "Wix Logo Maker", image: "illustrator", link: "https://chat.openai.com/")
        let model2 = AIModel(name: "Logomaster", image: "illustrator", link: "https://openai.com/blog/openai-codex")
        let model3 = AIModel(name: "Looka", image: "illustrator", link: "https://www.tabnine.com/")
        let model4 = AIModel(name: "LogoAi", image: "illustrator", link: "https://wpcode.com/")
        let model5 = AIModel(name: "Brandmark", image: "illustrator", link: "https://openai.com/blog/openai-codex")
        let model6 = AIModel(name: "Tailor Brands Logo Maker", image: "illustrator", link: "https://www.tabnine.com/")
        let model7 = AIModel(name: " Fiverr Logo Maker", image: "illustrator", link: "https://wpcode.com/")

        
        models.append(contentsOf: [model1, model2, model3,model4, model5, model6, model7])
        return models
    }
    func getAdAIModels() -> [AIModel] {
        var models = [AIModel]()
        
        let model1 = AIModel(name: "adcreative.ai", image: "tech", link: "https://chat.openai.com/")
        let model2 = AIModel(name: "copy.al", image: "tech", link: "https://openai.com/blog/openai-codex")
        let model3 = AIModel(name: "copysmith.ai", image: "tech", link: "https://www.tabnine.com/")
        models.append(contentsOf: [model1, model2, model3])
        return models
    }
    
    func getAnimationAIModels() -> [AIModel] {
        var models = [AIModel]()
        
        let model1 = AIModel(name: "Vyond", image: "animation", link: "https://chat.openai.com/")
        let model2 = AIModel(name: "Neural Frames", image: "animation", link: "https://openai.com/blog/openai-codex")
        let model3 = AIModel(name: "DeepBrain", image: "animation", link: "https://www.tabnine.com/")
        let model4 = AIModel(name: "VideoScribe", image: "animation", link: "https://wpcode.com/")
        let model5 = AIModel(name: "RenderForest", image: "animation", link: "https://openai.com/blog/openai-codex")
        let model6 = AIModel(name: "Doodly", image: "animation", link: "https://www.tabnine.com/")
        let model7 = AIModel(name: "Moovly", image: "animation", link: "https://wpcode.com/")
        let model8 = AIModel(name: "Animaker", image: "animation", link: "https://openai.com/blog/openai-codex")
        
        models.append(contentsOf: [model1, model2, model3,model4, model5, model6, model7,model8])
        return models
    }
    
    func getSubtitleAIModels() -> [AIModel] {
        var models = [AIModel]()
        
        let model1 = AIModel(name: "Flexclip", image: "subtitles", link: "https://chat.openai.com/")
        let model2 = AIModel(name: "Flixier", image: "subtitles", link: "https://openai.com/blog/openai-codex")
        let model3 = AIModel(name: "Kapwing", image: "subtitles", link: "https://www.tabnine.com/")
        
        models.append(contentsOf: [model1, model2, model3])
        return models
    }
    
    func getMemesAIModels() -> [AIModel] {
        var models = [AIModel]()
        
        let model1 = AIModel(name: "Imgflip", image: "smiley", link: "https://chat.openai.com/")
        let model2 = AIModel(name: "Super Meme", image: "smiley", link: "https://openai.com/blog/openai-codex")
        let model3 = AIModel(name: "Craiyon", image: "smiley", link: "https://www.tabnine.com/")
        let model4 = AIModel(name: "makememe", image: "smiley", link: "https://wpcode.com/")
       
        
        models.append(contentsOf: [model1, model2, model3,model4])
        return models
    }
    
    func getVideoEditingAIModels() -> [AIModel] {
        var models = [AIModel]()
        
        let model1 = AIModel(name: "Descript", image: "ve", link: "https://chat.openai.com/")
        let model2 = AIModel(name: "Wondershare Filmora", image: "ve", link: "https://openai.com/blog/openai-codex")
        let model3 = AIModel(name: "Runway", image: "ve", link: "https://www.tabnine.com/")
        let model4 = AIModel(name: "Peech", image: "ve", link: "https://wpcode.com/")
        let model5 = AIModel(name: "Synthesia", image: "ve", link: "https://openai.com/blog/openai-codex")
        let model6 = AIModel(name: "Fliki", image: "ve", link: "https://www.tabnine.com/")
        let model7 = AIModel(name: "Visla", image: "ve", link: "https://wpcode.com/")
        let model8 = AIModel(name: "Opus Clip", image: "ve", link: "https://openai.com/blog/openai-codex")
        
        models.append(contentsOf: [model1, model2, model3,model4, model5, model6, model7,model8])
        return models
    }
    
    func getscriptToVideoAIModels() -> [AIModel] {
        var models = [AIModel]()
        
        let model1 = AIModel(name: "Jasper AI", image: "cinema", link: "https://chat.openai.com/")
        let model2 = AIModel(name: "Invideo", image: "cinema", link: "https://openai.com/blog/openai-codex")
        let model3 = AIModel(name: "Writecream", image: "cinema", link: "https://www.tabnine.com/")
        let model4 = AIModel(name: "Writesonic", image: "cinema", link: "https://wpcode.com/")
        let model5 = AIModel(name: "Rytr", image: "cinema", link: "https://openai.com/blog/openai-codex")
        let model6 = AIModel(name: "Sudowrite", image: "cinema", link: "https://www.tabnine.com/")
        let model7 = AIModel(name: "AI Screenwriter", image: "cinema", link: "https://wpcode.com/")
        
        models.append(contentsOf: [model1, model2, model3,model4, model5, model6, model7])
        return models
    }
    
}
