//
//  AIForCoder.swift
//  SiteSense AI
//
//  Created by Nick Joliya on 30/09/23.
//

import UIKit

class AIForCoder: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var CVSimpleDesign: UICollectionView!
    var data = [AIModel]()
    var titleString = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        CVSimpleDesign.delegate = self
        CVSimpleDesign.dataSource = self
        
        lblTitle.text = titleString
        
       // data = AIDataManager.shared.getCoderAIModels()
        CVSimpleDesign.register(UINib(nibName: "CVCai", bundle: nil), forCellWithReuseIdentifier: "CVCai")
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

@available(iOS 13.0, *)
extension AIForCoder : UICollectionViewDataSource ,UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CVCai", for: indexPath) as! CVCai
        cell.imgBG.image = UIImage(named: data[indexPath.row].image)
        cell.lbltitle.text = data[indexPath.row].name
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width / 2 , height: collectionView.frame.size.height / 4)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}
